import { Component } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { ProdutoDetailPage } from '../produto-detail/produto-detail';
import { ProdutoProvider } from '../../providers/produto-provider';
import { Produto } from '../../model/produto';

@Component({
  selector: 'page-wishlist',
  templateUrl: 'wishlist.html'
})
export class WishlistPage {

  Produtos: Produto[];

  constructor(public navCtrl: NavController, public events: Events, public ProdutosProvider: ProdutoProvider) {
    // Filter produto data instead of showing entire set
    this.filterProdutoData();
    // Subscribe data changes to reapply filter
    this.events.subscribe('produto-data-changed', (produto) => {
      this.filterProdutoData();
    });
  }

  filterProdutoData() {
    this.produtos = this.produtosProvider.produtos.filter((produto) => {
      // return true if wishlist flag is true, show and not filter
      return produto.wishlist;
    });
  }

  openProdutoDetail(produto: any) {
    this.navCtrl.push(ProdutoDetailPage, { selectedProduto: produto });
  }
}

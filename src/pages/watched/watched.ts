import { Component } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { MovieDetailPage } from '../produto-detail/produto-detail';
import { ProdutoProvider } from '../../providers/produto-provider';
import { Movie } from '../../model/produto';

@Component({
  selector: 'page-watched',
  templateUrl: 'watched.html'
})
export class WatchedPage {

  movies: Movie[];

  constructor(public navCtrl: NavController, public events: Events, public moviesProvider: ProdutoProvider) {
    // Filter movie data instead of showing entire set
    this.filterMovieData();
    // Subscribe data changes to reapply filter
    this.events.subscribe('movie-data-changed', (movie) => {
      this.filterMovieData();
    });
  }

  filterMovieData() {
    // TODO: Filter this.moviesProvider.movies and only show the ones that have the watched flag set to true
  }

  openMovieDetail(movie: any) {
    this.navCtrl.push(MovieDetailPage, { selectedMovie: movie });
  }

}

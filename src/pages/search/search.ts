import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProdutoDetailPage } from '../produto-detail/produto-detail';
import { AddProdutoPage } from '../add-produto/add-produto';
import { ProdutoProvider } from '../../providers/produto-provider';
import { Produto } from '../../model/produto';

@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {

  produtos: Produto[];

  constructor(
    public navCtrl: NavController,
    public produtosProvider: ProdutoProvider
  ) {
    this.produtos = this.produtosProvider.produtos;
  }

  openProdutoDetail(produto: any) {
    this.navCtrl.push(ProdutoDetailPage, { selectedProduto: produto });
  }

  openProdutoAdd() {
    this.navCtrl.push(AddProdutoPage, {});
  }


  onSearchInputChanged(event: any) {
    // get the value of the searchbar and log it
    let searchQuery = event.target.value;
    console.log("onSearchInputChanged '" + searchQuery + "'");

    //TODO: Finish implementation of search functionality
  }

  addWish(produto: Produto) {
    //TODO: Implement change of wishlist flag
  }

  addWatched(produto: Produto) {
    //TODO: Implement change of watched flag
  }
}

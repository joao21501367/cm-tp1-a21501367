import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, NavParams, ViewController, ToastController} from 'ionic-angular';
import { ProdutoProvider } from '../../providers/produto-provider';
import { Produto } from '../../model/produto';

@Component({
  selector: 'page-add-produto',
  templateUrl: 'add-produto.html',
})
export class AddProdutoPage {

  produtoForm: any;
  submitAttempt: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public toastCtrl: ToastController,
    public formBuilder: FormBuilder,
    public produtosProvider: ProdutoProvider
  ) {

    // Tutorial with more complex FormBuilder and Validators at https://www.joshmorony.com/advanced-forms-validation-in-ionic-2/
    this.produtoForm = formBuilder.group({
        nome: ['', Validators.required],
        price: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]{4}')])],
        thumb: ['', Validators.required],
        sinopse: ['', Validators.required],
        runtime: ['', Validators.required],
        director: [''],
        stars: [''],
        genre: ['']
    });
  }


  saveProduto() {
    // Flag used to avoid displaying errors on first form appearance
    this.submitAttempt = true;

    // Validate form
    if(!this.produtoForm.valid){
      console.log("Error on Form!")
      this.showToastNotification("Error on Form!");
    }
    else {
      // Create produto object
      let newproduto = new produto(
        this.produtoForm.value.nome,
        this.produtoForm.value.thumb,
        this.produtoForm.value.price,
        this.produtoForm.value.sinopse,
        null, // Missing input for produto Director
        this.produtoForm.value.runtime,
        null, // Missing input for produto Stars
        null, // Missing input for produto Genre
        false,
        false
      );
      // Save produto in provider and publish change
      this.produtosProvider.save(newProduto);
      this.produtosProvider.publishChange(newproduto);

      // Show notification and log
      console.log("Produto '" + newProduto.nome + "' Saved!")
      this.showToastNotification("Produto '" + newProduto.nome + "' Saved!");
      // Go back to previous screen
      this.viewCtrl.dismiss();
    }
  }


  showToastNotification(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

}

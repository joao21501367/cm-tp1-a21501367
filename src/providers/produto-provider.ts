import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Events } from 'ionic-angular';
import { Movie } from '../model/produto';


/*
  Generated class for the MoviesProvider provider.
  See https://ionicframework.com/docs/cli/generate/

  See https://angular.io/guide/dependency-injection for more info on providers and Angular DI.

  For a tutorial about providers check https://www.joshmorony.com/an-in-depth-explanation-of-providers-in-ionic-2/
*/
@Injectable()
export class ProdutoProvider {

  public movies: Movie[];

  constructor(private http: Http, public events : Events) {
    this.movies = [];
    this.initializeData();
  }


  initializeData() {
    // Tutorial on HTTP Data Fetch https://www.joshmorony.com/using-http-to-fetch-remote-data-from-a-server-in-ionic-2/
    // Asynchronous call, code will resume below
    this.http.get('assets/data/produto.json').map(res => res.json()).subscribe(
      data => {
        for (var i=0; i < data.length; ++i) {
          this.movies.push(
            new Movie(data[i].title, data[i].thumb, data[i].price, data[i].sinopse, data[i].marca, data[i].runtime, data[i].stars, data[i].genre, data[i].watched, data[i].wishlist)
          );
        }
      },
      err => {
          console.log("Error Loading Data!");
      }
    );
  }

  save(movie: Movie) {
    this.movies.push(movie);
    console.log("Movie '" + movie.title + "' saved in-memory!");
  }

  publishChange(movie: Movie) {
    // More info on how to use Events at https://ionicframework.com/docs/api/util/Events/
    this.events.publish('movie-data-changed', movie );
    console.log("Publishing event 'movie-data-changed'");
  }

}

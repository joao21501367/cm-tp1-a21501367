webpackJsonp([0],{

/***/ 111:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 111;

/***/ }),

/***/ 154:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 154;

/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__search_search__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__watched_watched__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__wishlist_wishlist__ = __webpack_require__(203);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TabsPage = (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_1__search_search__["a" /* SearchPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_3__wishlist_wishlist__["a" /* WishlistPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__watched_watched__["a" /* WatchedPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/joaoneves/Desktop/TPC-CM/src/pages/tabs/tabs.html"*/'<ion-tabs>\n  <ion-tab [root]="tab1Root" tabTitle=" Items " tabIcon="search"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="Carrinho" tabIcon="cart"></ion-tab>\n\n</ion-tabs>\n'/*ion-inline-end:"/Users/joaoneves/Desktop/TPC-CM/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__produto_detail_produto_detail__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__add_produto_add_produto__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_produto_provider__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SearchPage = (function () {
    function SearchPage(navCtrl, moviesProvider) {
        this.navCtrl = navCtrl;
        this.moviesProvider = moviesProvider;
        this.movies = this.moviesProvider.movies;
    }
    SearchPage.prototype.openMovieDetail = function (movie) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__produto_detail_produto_detail__["a" /* MovieDetailPage */], { selectedMovie: movie });
    };
    SearchPage.prototype.openMovieAdd = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__add_produto_add_produto__["a" /* AddMoviePage */], {});
    };
    SearchPage.prototype.onSearchInputChanged = function (event) {
        // get the value of the searchbar and log it
        var searchQuery = event.target.value;
        console.log("onSearchInputChanged '" + searchQuery + "'");
        //TODO: Finish implementation of search functionality
    };
    SearchPage.prototype.addWish = function (movie) {
        //TODO: Implement change of wishlist flag
    };
    SearchPage.prototype.addWatched = function (movie) {
        //TODO: Implement change of watched flag
    };
    SearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-search',template:/*ion-inline-start:"/Users/joaoneves/Desktop/TPC-CM/src/pages/search/search.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Itenss</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-searchbar [(ngModel)]="searchQuery" (ionInput)="onSearchInputChanged($event)"></ion-searchbar>\n\n  <ion-list>\n    <ion-item-sliding *ngFor="let movie of this.movies">\n      <ion-item detail-push (click)="openMovieDetail(movie)">\n        <ion-thumbnail item-start class="movie-cover">\n          <img src="{{ movie.thumb }}">\n        </ion-thumbnail>\n        <h2>{{ movie.title }}</h2>\n        <p>{{ movie.genre }}</p>\n        <p>{{ movie.director }}</p>\n        <p>{{ movie.price }}</p>\n      </ion-item>\n      <ion-item-options side="right">\n        <button ion-button color="primary" (click)="addWish(movie)">\n          <ion-icon name="cart"></ion-icon>\n          Add Wish\n        </button>\n        <button ion-button color="secondary" (click)="addWatched(movie)">\n          <ion-icon name="flag"></ion-icon>\n          Watched\n        </button>\n      </ion-item-options>\n    </ion-item-sliding>\n  </ion-list>\n\n  <ion-fab right bottom>\n    <button ion-fab>\n      <ion-icon name="add"></ion-icon>\n    </button>\n  </ion-fab>\n\n</ion-content>\n'/*ion-inline-end:"/Users/joaoneves/Desktop/TPC-CM/src/pages/search/search.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_produto_provider__["a" /* ProdutoProvider */]])
    ], SearchPage);
    return SearchPage;
}());

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Movie; });
var Movie = (function () {
    function Movie(title, thumb, price, sinopse, Marca, runtime, stars, genre, watched, wishlist) {
        this.title = title;
        this.thumb = thumb;
        this.price = price;
        this.sinopse = sinopse;
        this.Marca = Marca;
        this.runtime = runtime;
        this.stars = stars;
        this.genre = genre;
        this.watched = watched;
        this.wishlist = wishlist;
    }
    return Movie;
}());

//# sourceMappingURL=produto.js.map

/***/ }),

/***/ 201:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddMoviePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_produto_provider__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__model_produto__ = __webpack_require__(200);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddMoviePage = (function () {
    function AddMoviePage(navCtrl, navParams, viewCtrl, toastCtrl, formBuilder, moviesProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.formBuilder = formBuilder;
        this.moviesProvider = moviesProvider;
        // Tutorial with more complex FormBuilder and Validators at https://www.joshmorony.com/advanced-forms-validation-in-ionic-2/
        this.movieForm = formBuilder.group({
            title: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
            price: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].pattern('[0-9]{4}')])],
            thumb: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
            sinopse: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
            runtime: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
            director: [''],
            stars: [''],
            genre: ['']
        });
    }
    AddMoviePage.prototype.saveMovie = function () {
        // Flag used to avoid displaying errors on first form appearance
        this.submitAttempt = true;
        // Validate form
        if (!this.movieForm.valid) {
            console.log("Error on Form!");
            this.showToastNotification("Error on Form!");
        }
        else {
            // Create movie object
            var newMovie = new __WEBPACK_IMPORTED_MODULE_4__model_produto__["a" /* Movie */](this.movieForm.value.title, this.movieForm.value.thumb, this.movieForm.value.price, this.movieForm.value.sinopse, null, // Missing input for movie Director
            this.movieForm.value.runtime, null, // Missing input for movie Stars
            null, // Missing input for movie Genre
            false, false);
            // Save movie in provider and publish change
            this.moviesProvider.save(newMovie);
            this.moviesProvider.publishChange(newMovie);
            // Show notification and log
            console.log("Movie '" + newMovie.title + "' Saved!");
            this.showToastNotification("Movie '" + newMovie.title + "' Saved!");
            // Go back to previous screen
            this.viewCtrl.dismiss();
        }
    };
    AddMoviePage.prototype.showToastNotification = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    AddMoviePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-add-produto',template:/*ion-inline-start:"/Users/joaoneves/Desktop/TPC-CM/src/pages/add-produto/add-produto.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Add Protuto</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <form [formGroup]="produtoForm">\n    <ion-item>\n      <ion-icon name="shirt" item-start> </ion-icon>\n        <ion-label>Marca</ion-label>\n        <ion-input formControlName="title" type="text"></ion-input>\n        <ion-icon name="alert" class="error" item-end *ngIf="!produtoForm.controls.title.valid && submitAttempt"> </ion-icon>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-icon name="image" item-start> </ion-icon>\n      <ion-label>Cover Image</ion-label>\n      <ion-input formControlName="thumb" type="text"></ion-input>\n      <ion-icon name="alert" class="error" item-end *ngIf="!produtoForm.controls.thumb.valid && submitAttempt"> </ion-icon>\n    </ion-item>\n\n    <ion-item>\n      <ion-icon name="paper" item-start> </ion-icon>\n      <ion-label>Descrição</ion-label>\n      <ion-input formControlName="sinopse" type="text"></ion-input>\n      <ion-icon name="alert" class="error" item-end *ngIf="!produtoForm.controls.sinopse.valid && submitAttempt"> </ion-icon>\n    </ion-item>\n\n\n\n    <!-- Missing input for produto Director -->\n\n    <!-- Missing input for produto Stars -->\n\n    <!-- Missing input for produto Genre -->\n\n    <div padding>\n      <button ion-button type="submit" full round (click)="saveProduto()">Add Produto</button>\n    </div>\n  </form>\n\n</ion-content>\n'/*ion-inline-end:"/Users/joaoneves/Desktop/TPC-CM/src/pages/add-produto/add-produto.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3__providers_produto_provider__["a" /* ProdutoProvider */]])
    ], AddMoviePage);
    return AddMoviePage;
}());

//# sourceMappingURL=add-produto.js.map

/***/ }),

/***/ 202:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WatchedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__produto_detail_produto_detail__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_produto_provider__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var WatchedPage = (function () {
    function WatchedPage(navCtrl, events, moviesProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.events = events;
        this.moviesProvider = moviesProvider;
        // Filter movie data instead of showing entire set
        this.filterMovieData();
        // Subscribe data changes to reapply filter
        this.events.subscribe('movie-data-changed', function (movie) {
            _this.filterMovieData();
        });
    }
    WatchedPage.prototype.filterMovieData = function () {
        // TODO: Filter this.moviesProvider.movies and only show the ones that have the watched flag set to true
    };
    WatchedPage.prototype.openMovieDetail = function (movie) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__produto_detail_produto_detail__["a" /* MovieDetailPage */], { selectedMovie: movie });
    };
    WatchedPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-watched',template:/*ion-inline-start:"/Users/joaoneves/Desktop/TPC-CM/src/pages/watched/watched.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Watched\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <button ion-item *ngFor="let movie of this.movies" (click)="openMovieDetail(movie)">\n      <ion-thumbnail item-start class="movie-cover">\n        <img src="{{ movie.thumb }}">\n      </ion-thumbnail>\n      <h2>{{movie.title}}</h2>\n      <p>{{movie.year}}</p>\n      <p>{{ movie.stars }}</p>\n    </button>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/joaoneves/Desktop/TPC-CM/src/pages/watched/watched.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Events */], __WEBPACK_IMPORTED_MODULE_3__providers_produto_provider__["a" /* ProdutoProvider */]])
    ], WatchedPage);
    return WatchedPage;
}());

//# sourceMappingURL=watched.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WishlistPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__produto_detail_produto_detail__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_produto_provider__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var WishlistPage = (function () {
    function WishlistPage(navCtrl, events, ProdutosProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.events = events;
        this.ProdutosProvider = ProdutosProvider;
        // Filter produto data instead of showing entire set
        this.filterProdutoData();
        // Subscribe data changes to reapply filter
        this.events.subscribe('produto-data-changed', function (produto) {
            _this.filterProdutoData();
        });
    }
    WishlistPage.prototype.filterProdutoData = function () {
        this.produtos = this.produtosProvider.produtos.filter(function (produto) {
            // return true if wishlist flag is true, show and not filter
            return produto.wishlist;
        });
    };
    WishlistPage.prototype.openProdutoDetail = function (produto) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__produto_detail_produto_detail__["ProdutoDetailPage"], { selectedProduto: produto });
    };
    WishlistPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-wishlist',template:/*ion-inline-start:"/Users/joaoneves/Desktop/TPC-CM/src/pages/wishlist/wishlist.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Carrinho\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <button ion-item *ngFor="let movie of this.movies" (click)="openMovieDetail(movie)">\n      <ion-thumbnail item-start class="movie-cover">\n        <img src="{{ movie.thumb }}">\n      </ion-thumbnail>\n      <h2>{{movie.title}}</h2>\n      <p>{{movie.year}}</p>\n      <p>{{ movie.stars }}</p>\n    </button>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/joaoneves/Desktop/TPC-CM/src/pages/wishlist/wishlist.html"*/
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Events */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Events */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__providers_produto_provider__["a" /* ProdutoProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_produto_provider__["a" /* ProdutoProvider */]) === "function" && _c || Object])
    ], WishlistPage);
    return WishlistPage;
    var _a, _b, _c;
}());

//# sourceMappingURL=wishlist.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(228);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 228:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_tabs_tabs__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_search_search__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_watched_watched__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_wishlist_wishlist__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_produto_detail_produto_detail__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_add_produto_add_produto__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_status_bar__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_splash_screen__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_produto_provider__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_watched_watched__["a" /* WatchedPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_wishlist_wishlist__["a" /* WishlistPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_produto_detail_produto_detail__["a" /* MovieDetailPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_add_produto_add_produto__["a" /* AddMoviePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {}, {
                    links: []
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_watched_watched__["a" /* WatchedPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_wishlist_wishlist__["a" /* WishlistPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_produto_detail_produto_detail__["a" /* MovieDetailPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_add_produto_add_produto__["a" /* AddMoviePage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_13__providers_produto_provider__["a" /* ProdutoProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 270:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_produto_provider__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyApp = (function () {
    //  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    function MyApp(platform, statusBar, splashScreen, produtosProvider) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/joaoneves/Desktop/TPC-CM/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/joaoneves/Desktop/TPC-CM/src/app/app.html"*/,
            // Provider Injected globally. Will share state across different Components (pages)
            providers: [__WEBPACK_IMPORTED_MODULE_5__providers_produto_provider__["a" /* ProdutoProvider */]]
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_5__providers_produto_provider__["a" /* ProdutoProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__providers_produto_provider__["a" /* ProdutoProvider */]) === "function" && _d || Object])
    ], MyApp);
    return MyApp;
    var _a, _b, _c, _d;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 30:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProdutoProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__model_produto__ = __webpack_require__(200);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the MoviesProvider provider.
  See https://ionicframework.com/docs/cli/generate/

  See https://angular.io/guide/dependency-injection for more info on providers and Angular DI.

  For a tutorial about providers check https://www.joshmorony.com/an-in-depth-explanation-of-providers-in-ionic-2/
*/
var ProdutoProvider = (function () {
    function ProdutoProvider(http, events) {
        this.http = http;
        this.events = events;
        this.movies = [];
        this.initializeData();
    }
    ProdutoProvider.prototype.initializeData = function () {
        var _this = this;
        // Tutorial on HTTP Data Fetch https://www.joshmorony.com/using-http-to-fetch-remote-data-from-a-server-in-ionic-2/
        // Asynchronous call, code will resume below
        this.http.get('assets/data/produto.json').map(function (res) { return res.json(); }).subscribe(function (data) {
            for (var i = 0; i < data.length; ++i) {
                _this.movies.push(new __WEBPACK_IMPORTED_MODULE_4__model_produto__["a" /* Movie */](data[i].title, data[i].thumb, data[i].price, data[i].sinopse, data[i].marca, data[i].runtime, data[i].stars, data[i].genre, data[i].watched, data[i].wishlist));
            }
        }, function (err) {
            console.log("Error Loading Data!");
        });
    };
    ProdutoProvider.prototype.save = function (movie) {
        this.movies.push(movie);
        console.log("Movie '" + movie.title + "' saved in-memory!");
    };
    ProdutoProvider.prototype.publishChange = function (movie) {
        // More info on how to use Events at https://ionicframework.com/docs/api/util/Events/
        this.events.publish('movie-data-changed', movie);
        console.log("Publishing event 'movie-data-changed'");
    };
    ProdutoProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* Events */]])
    ], ProdutoProvider);
    return ProdutoProvider;
}());

//# sourceMappingURL=produto-provider.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MovieDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_produto_provider__ = __webpack_require__(30);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MovieDetailPage = (function () {
    function MovieDetailPage(navParams, events, moviesProvider) {
        this.navParams = navParams;
        this.events = events;
        this.moviesProvider = moviesProvider;
        this.movie = this.navParams.get('selectedMovie');
        console.log("Showing detail for movie '" + this.movie.title + "'");
    }
    MovieDetailPage.prototype.toggleChanged = function () {
        // TODO: Make sure change is published so that subcribers can reload data.
        // Current subscribers are Watched and Wishlist Pages.
        // See Events https://ionicframework.com/docs/api/util/Events/
    };
    MovieDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-produto-detail',template:/*ion-inline-start:"/Users/joaoneves/Desktop/TPC-CM/src/pages/produto-detail/produto-detail.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n        Informação sobre o Produto\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <div padding>\n    <h1>{{produto.title}}</h1>\n    <p> {{produto.sinopse}} </p>\n  </div>title\n\n  <ion-list>\n    <ion-item>\n      <ion-icon name="shirt" item-start> </ion-icon>\n      <h2>Marca</h2>\n      <p item-right> {{produto.director}} </p>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-icon name="pricetags" item-start> </ion-icon>\n      <h2>Preço</h2>\n      <p item-right> {{produto.price}} </p>\n    </ion-item>\n  </ion-list>\n\n  <ion-list>\n    <ion-item>\n      <ion-icon name="cart" item-start> </ion-icon>\n      <ion-label>Adicionar no Carrinho</ion-label>\n      <ion-toggle [(ngModel)]="produto.wishlist" (ionChange)="toggleChanged()"></ion-toggle>\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/joaoneves/Desktop/TPC-CM/src/pages/produto-detail/produto-detail.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Events */], __WEBPACK_IMPORTED_MODULE_2__providers_produto_provider__["a" /* ProdutoProvider */]])
    ], MovieDetailPage);
    return MovieDetailPage;
}());

//# sourceMappingURL=produto-detail.js.map

/***/ })

},[204]);
//# sourceMappingURL=main.js.map